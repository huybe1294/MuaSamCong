﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MSC
{
	public partial class App : Application
	{
        public static NavigationPage NavigationPage { get; private set; }
        public static Page RootPage { get; private set; }
        public App ()
		{
			InitializeComponent();

            Page MenuPage = new HomeMaster();
            NavigationPage = new NavigationPage(new HomeDetail());
            RootPage = new Home
            {
                Master = MenuPage,
                Detail = NavigationPage
            };

            MainPage = RootPage;
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
