﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using MSC.View;
using Xamarin.Forms.Xaml;

namespace MSC
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeDetail : ContentPage
    {
        public HomeDetail()
        {
            InitializeComponent();

        }

        //show master page
        private void OpenMasTerPage(object sender, EventArgs args)
        {
            try
            {
                var mdPage = App.Current.MainPage as MasterDetailPage;
                mdPage.IsPresented = true;
            }
            catch (Exception e)
            {
               throw e;
            }
        }

    }
}