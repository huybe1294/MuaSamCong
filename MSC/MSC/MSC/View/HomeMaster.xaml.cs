﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MSC.View;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSC
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeMaster : ContentPage
    {
        public ListView ListView;

        public HomeMaster()
        {
            InitializeComponent();

            BindingContext = new HomeMasterViewModel();
            ListView = MenuItemsListView;

        }

        //button login on clicked
       async void OnLoginTapped(object sender, EventArgs args)
        {
            try
            {
                var mdPage = App.Current.MainPage as MasterDetailPage;
                mdPage.IsPresented = false;

                await App.NavigationPage.PushAsync(new Login());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        class HomeMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<HomeMenuItem> MenuItems { get; set; }
            
            public HomeMasterViewModel()
            {
                MenuItems = new ObservableCollection<HomeMenuItem>(new[]
                {
                    new HomeMenuItem { Id = 0, Title = "Kế hoạch lựa chọn nhà thầu" , PhotoUrl="ic_nav_ke_hoach.png"},
                    new HomeMenuItem { Id = 1, Title = "Thông tin đấu thầu" ,PhotoUrl="ic_nav_thong_tin.png"},
                    new HomeMenuItem { Id = 2, Title = "Kết quả" ,PhotoUrl="ic_nav_ket_qua.png"},
                    new HomeMenuItem { Id = 3, Title = "Tin đánh dấu",PhotoUrl="ic_nav_tin_danh_dau.png" },
                    new HomeMenuItem { Id = 4, Title = "Thông tin chọn lọc" ,PhotoUrl="ic_nav_thong_tin.png"},
                    new HomeMenuItem { Id = 5, Title = "Thông báo từ bộ KH-ĐT" ,PhotoUrl="ic_nav_thong_bao.png"},
                    new HomeMenuItem { Id = 6, Title = "Tùy chỉnh" ,PhotoUrl="ic_nav_tuy_chinh.png"},
                    new HomeMenuItem { Id = 7, Title = "Hỗ trợ" ,PhotoUrl="ic_nav_ho_tro.png"},
                });
            }
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}